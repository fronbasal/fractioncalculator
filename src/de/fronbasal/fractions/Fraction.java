package de.fronbasal.fractions;

/**
 * Fraction abstraction class
 *
 * @author fronbasal
 */
final class Fraction extends Number {
    private int numerator;
    private int denominator;

    /**
     * @param numerator   numerator of the fraction
     * @param denominator denominator of the fraction
     */
    Fraction(int numerator, int denominator) {
        if (denominator == 0) {
            throw new IllegalArgumentException("denominator is zero");
        }
        if (denominator < 0) {
            numerator *= -1;
            denominator *= -1;
        }
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction(int numerator) {
        this.numerator = numerator;
        this.denominator = 1;
    }

    public int getNumerator() {
        return this.numerator;
    }

    public int getDenominator() {
        return this.denominator;
    }

    public double doubleValue() {
        return ((double) numerator) / ((double) denominator);
    }

    public float floatValue() {
        return (float) this.doubleValue();
    }

    public int intValue() {
        return (int) this.doubleValue();
    }

    public long longValue() {
        return (long) this.doubleValue();
    }

    public short shortValue() {
        return (short) this.doubleValue();
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    /**
     * @return a string representation of the fraction
     */
    @Override
    public String toString() {
        return this.getNumerator() + " / " + this.getDenominator();
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }
}
