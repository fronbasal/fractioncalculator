package de.fronbasal.fractions;

/**
 * FractionCalculator superclass
 *
 * @author fronbasal
 */
public class FractionCalculator {
    private Fraction fraction;


    /**
     * Constructor of this object
     *
     * @param fraction fraction
     */
    FractionCalculator(Fraction fraction) {
        this.fraction = fraction;
    }

    /**
     * Sets the fraction of this object
     *
     * @param fraction the fraction to set
     */
    public void setFraction(Fraction fraction) {
        this.fraction = fraction;
    }


    /**
     * Returns the fraction of this object
     *
     * @return fraction
     */
    public Fraction getFraction() {
        return fraction;
    }

    /**
     * Multiplies a fraction
     *
     * @param n fraction to multiply
     * @return the multiplied fraction
     */
    public Fraction multiplyFraction(Fraction n) {
        return new Fraction(this.fraction.getNumerator() * n.getNumerator(), this.fraction.getDenominator() * n.getDenominator());
    }

    /**
     * Divides a fraction
     *
     * @param n fraction to divide
     * @return the divided fraction
     */
    public Fraction divideFraction(Fraction n) {
        int y = n.getDenominator();
        n.setDenominator(n.getNumerator());
        n.setNumerator(y);
        return this.multiplyFraction(n);
    }

    /**
     * Shortens a fraction
     *
     * @param n fraction to shorten
     * @return the shortened fraction
     */
    public Fraction shortenFraction(Fraction n) {
        int x = calculateGreatestCommonDivider(n);
        return new Fraction(n.getNumerator() / x, n.getDenominator() / x);
    }

    /**
     * Calculate the greatest common divider
     *
     * @param n fraction
     * @return the calculated greatest common divider
     */
    private int calculateGreatestCommonDivider(Fraction n) {
        if (n.getDenominator() < 0) n.setDenominator(-n.getDenominator());
        if (n.getNumerator() < 0) n.setNumerator(-n.getNumerator());

        int max = Math.max(n.getNumerator(), n.getDenominator());
        int min = Math.min(n.getNumerator(), n.getDenominator());

        if (min == 0) return max;

        for (int i = 1; i != 0; ) {
            i = max % min;
            max = min;
            min = i;
        }
        return max;
    }
}
