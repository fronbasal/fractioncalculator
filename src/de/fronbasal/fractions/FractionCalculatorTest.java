package de.fronbasal.fractions;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class FractionCalculatorTest {
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(FractionCalculator.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @org.junit.Test
    public void multiplyFraction() {
        FractionCalculator c = new FractionCalculator(new Fraction(1, 2));
        Fraction x = c.multiplyFraction(new Fraction(1, 2));
        assert x.doubleValue() == 0.125;
    }

    @org.junit.Test
    public void divideFraction() {
    }
}
