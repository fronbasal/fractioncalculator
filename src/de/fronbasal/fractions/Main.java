package de.fronbasal.fractions;

public class Main {
    public static void main(String[] args) {
        FractionCalculator c = new FractionCalculator(new Fraction(1, 2));
        Fraction m = c.multiplyFraction(new Fraction(1, 2));
        System.out.println("Multiplying 1/2 with 1/2: " + m);
        System.out.println("Shortened form: " + c.shortenFraction(m));

        Fraction d = c.divideFraction(new Fraction(1, 6));
        System.out.println("Dividing 1/2 with 1/6: " + d);
        System.out.println("Shortened form: " + c.shortenFraction(d));
    }
}
